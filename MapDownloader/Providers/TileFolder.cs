﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace MapDownloader.Providers
{
    /// <summary>
    /// Creates folder structure for tile images based on template string
    /// </summary>
    class TileFolder : ITileWriter, ITileProvider
    {
        private String rootPath;
        private TileTemplate template;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootPath">path to root directory containing tile directory structure</param>
        /// <param name="directoryTemplate">directory template for tiles. Use {zoom}, {x}, {y}. Example: <code>"{zoom}/{x}/{y}.png"</code></param>
        public TileFolder(String rootPath, String directoryTemplate)
        {
            this.rootPath = rootPath;
            this.template = new TileTemplate(Path.Combine(this.rootPath, directoryTemplate), "{x}", "{y}", "{zoom}");
            //Directory.CreateDirectory(this.rootPath);
        }

        /// <summary>
        /// Returns path to tile including root directory
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        private String getTilePath(int x, int y, int zoom) {
            return this.template.GetString(x, y, zoom); 
        }

        /// <summary>
        /// Returns true when tile file exists
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public bool Exists(int x, int y, int zoom)
        {
            return File.Exists(getTilePath(x, y, zoom));
        }


        /// <summary>
        /// Stores tile, does not overwrite existing tile.
        /// <see cref="StoreTile(Stream, int, int, int, bool)"/>
        /// </summary>
        /// <param name="tileStream"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        public void StoreTile(Stream tileStream, int x, int y, int zoom)
        {
            StoreTile(tileStream, x, y, zoom, false);
        }

        /// <summary>
        /// Store tile to appropriate file based on tile coordinates. Does not overwrite existing tile files unless called with <code>overwrite</code> argument set to <code>true</code>.
        /// </summary>
        /// <param name="tileStream">Stream containing tile data</param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        /// <param name="overwrite">If true, then overwrite tile if it already exists.</param>
        public void StoreTile(Stream tileStream, int x, int y, int zoom, bool overwrite)
        {
            // if the tile already exists then do nothing
            if (!overwrite && Exists(x, y, zoom)) return;

            // get paths
            String tileFilePath = getTilePath(x, y, zoom);
            String tileDir = Path.GetDirectoryName(tileFilePath);

            

            // create necessary directories
            if (!Directory.Exists(tileDir)) {
                Directory.CreateDirectory(tileDir);
            }
            
            // save tile
            using(var fileStream = File.Create(tileFilePath)) {
                byte[] buffer = new byte[16 * 1024]; // Fairly arbitrary size
                int bytesRead;

                while ((bytesRead = tileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, bytesRead);
                }
            }

        }

        /// <summary>
        /// Returns tile stream for tile on given coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public Stream GetTileStream(int x, int y, int zoom)
        {
            return File.OpenRead(getTilePath(x, y, zoom));
        }

        public override string ToString()
        {
            return this.rootPath;
        }

    }
}
