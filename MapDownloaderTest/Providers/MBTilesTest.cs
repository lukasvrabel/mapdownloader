﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MapDownloader.Providers;
using System.IO;
using MapDownloaderTest.Properties;
using System.Drawing.Imaging;

namespace MapDownloaderTest.Providers
{
    [TestClass]
    public class MBTilesTest
    {
        [TestMethod]
        public void MBTilesSpec()
        {
            // test mbtiles database filename
            string fileName = "_testfile.mbtiles";

            // delete file in case it exists
            if (File.Exists(fileName)) 
            {
                File.Delete(fileName);
            }
            Assert.IsFalse(File.Exists(fileName));

            // create new mock tile provider to test tile writing
            MockTileProvider mock = new MockTileProvider();
            
            // create mb tiles and check if it is empty
            MBTiles tiles = new MBTiles(fileName);
            Assert.AreEqual(tiles.TileCount, 0);
            Assert.IsFalse(tiles.Exists(0, 0, 0));
            long fileSize = tiles.FileSize;

            // store tile
            Stream mockTile = mock.GetTileStream(0, 0, 0);
            tiles.StoreTile(mockTile, 0, 0, 0);
            Assert.AreEqual(tiles.TileCount, 1);
            Assert.IsTrue(tiles.FileSize > fileSize, "File Size was not increased after storeTile");
            Assert.IsTrue(tiles.Exists(0, 0, 0));
            Assert.IsFalse(tiles.Exists(0, 0, 1));

            // read tile and compare it to mock tile
            Stream storedTile = tiles.GetTileStream(0, 0, 0);
            //bool isEqual = mock.IsEqal(storedTile);
            //Assert.IsTrue(isEqual);
            Assert.IsTrue(Utils.checkStreamHeader(storedTile, ImageFormat.Jpeg));


            // create MBTiles from existing db
            tiles = new MBTiles(fileName);

            // check if the stored file is there
            Assert.AreEqual(tiles.TileCount, 1);
            storedTile = tiles.GetTileStream(0, 0, 0);
            //isEqual = mock.IsEqal(storedTile);
            Assert.IsTrue(Utils.checkStreamHeader(storedTile, ImageFormat.Jpeg));


            // overwrite existing db with new one
            tiles = new MBTiles(fileName: fileName, overwrite: true);
            Assert.AreEqual(tiles.TileCount, 0);

        }

        
    }
}
