﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MapDownloader
{
    /// <summary>
    /// Provides tile stream for tile at coordinates x, y, zoom
    /// </summary>
    public interface ITileProvider
    {
        Stream GetTileStream(int x, int y, int zoom);
    }

    /// <summary>
    /// writes tile image defined by tileStream
    /// </summary>
    public interface ITileWriter
    {
        bool Exists(int x, int y, int zoom);
        void StoreTile(Stream tileStream, int x, int y, int zoom);
    }

    public interface ITileEnumerator
    {
        IEnumerable<TileCoordinates> Coordinates { get; }
        int Count { get; }
    }

    public interface ITileDownloadWorker
    {
        bool Download(TileCoordinates coordinates);
    }

}
