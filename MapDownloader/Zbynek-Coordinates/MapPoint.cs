﻿using System;
using System.Collections.Generic;
using System.Text;
using Gina.Coordinates;
using System.Globalization;
using System.Diagnostics;
using System.Drawing;

namespace Gina.Coordinates
{
    public struct MapPoint
    {
        public static readonly MapPoint Empty = new MapPoint(0, 0, 0);

        public int X;
        public int Y;
        public int Zoom;
        public bool IsEmpty { get { return this.Equals(Empty); } }



        public static implicit operator Point(MapPoint val)
        {
            return new Point(val.X, val.Y);
        }


        public MapPoint(int x, int y, int zoom)
        {
            this.X = x;
            this.Y = y;
            this.Zoom = zoom;
        }


        /// <summary>
        /// Adjusts the coordinates to given zoom
        /// </summary>
        public MapPoint AdjustToZoom(int zoom)
        {
            if (zoom == this.Zoom)
                return this;

            return GeoPoint.FromMapPoint(this).ToMapPoint(zoom);
        }


        /// <summary>
        /// Offsets the position
        /// </summary>
        public void Offset(int x, int y)
        {
            this.X += x;
            this.Y += y;
        }


        /// <summary>
        /// Gets if the point equals the current point
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            MapPoint point = (MapPoint)obj;
            return this.X == point.X && this.Y == point.Y && this.Zoom == point.Zoom;
        }


        /// <summary>
        /// Gets the hash code of the object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.X.GetHashCode() ^ this.Y.GetHashCode();
        }


        /// <summary>
        /// Prevede bod na textovy retezec
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return X.ToString(CultureInfo.InvariantCulture) + ';' + Y.ToString(CultureInfo.InvariantCulture) + ';' + Zoom;
        }


        /// <summary>
        /// Vrati prislusnou cast mapy (bod [0,0] odpovida -180°long a cca 85°lat)
        /// </summary>
        /// <returns></returns>
        public MapTileInfo ToMapTileInfo()
        {
            return new MapTileInfo((int)(X / MapTileInfo.TileSize), (int)(Y / MapTileInfo.TileSize), Zoom);
        }


        /// <summary>
        /// Vrati linearni souradnice leveho horniho rohu
        /// </summary>
        /// <returns></returns>
        public static MapPoint FromMapTileInfo(MapTileInfo point)
        {
            return new MapPoint(point.X * MapTileInfo.TileSize, point.Y * MapTileInfo.TileSize, point.Zoom);
        }


        /// <summary>
        /// Converts the specified string representation of a linear map point to its MapPoint equivalent
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static MapPoint Parse(string s)
        {
            string[] parts = s.Split(';');
            return new MapPoint(int.Parse(parts[0], CultureInfo.InvariantCulture), int.Parse(parts[1], CultureInfo.InvariantCulture), int.Parse(parts[2]));
        }
    }
}
