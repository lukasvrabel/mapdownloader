﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Drawing;
using System.Diagnostics;

namespace MapDownloader
{
    /// <summary>
    /// Downloads files from source tile provider to destination tile writer.
    /// </summary>
    class TileDownloader
    {
        
        private Thread[] threads = new Thread[4];

        private ITileProvider tileSource;
        public ITileProvider Source { get { return this.tileSource;  } }

        private ITileWriter tileDestination;
        public ITileWriter Destination { get { return this.tileDestination; } }

        private int downloadedCount;
        public int DownloadedCount { get { return this.downloadedCount; } }

        private int maxCount;
        public int MaxCount { get{return this.maxCount;} }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tileSource">Tile provider</param>
        /// <param name="tileDestination">Tile storage writer</param>
        public TileDownloader(ITileProvider tileSource, ITileWriter tileDestination)
        {
            this.tileSource = tileSource;
            this.tileDestination = tileDestination;
        }

        /// <summary>
        /// Start downloading tiles in 4 threads. 
        /// Download tile at x, y, zoom and all tiles underneath it up to zoom level maxZoom (included). Downloads only tiles that does not exists in <code>tileDestination</code>.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        /// <param name="maxZoom"></param>
        public void RunDownloadTiles(int x, int y, int zoom, int maxZoom)
        {
            this.downloadedCount = 0;
            this.maxCount = TileCount(zoom, maxZoom);

            // download and save tile only if it does not exists already
            if (!this.tileDestination.Exists(x, y, zoom))
            {
                using (Stream tileStream = this.tileSource.GetTileStream(x, y, zoom))
                {
                    this.tileDestination.StoreTile(tileStream, x, y, zoom);

                }
            }
            
            this.downloadedCount++;
            if (zoom == maxZoom) return;

            // create thread for each sublevel
            this.threads[0] = new Thread(new ThreadStart(() => recurseDownload(2 * x, 2 * y, zoom + 1, maxZoom)));
            this.threads[1] = new Thread(new ThreadStart(() => recurseDownload(2 * x + 1, 2 * y, zoom + 1, maxZoom)));
            this.threads[2] = new Thread(new ThreadStart(() => recurseDownload(2 * x, 2 * y + 1, zoom + 1, maxZoom)));
            this.threads[3] = new Thread(new ThreadStart(() => recurseDownload(2 * x + 1, 2 * y + 1, zoom + 1, maxZoom)));

            // start threads
            foreach (Thread t in this.threads) t.Start();
            
            
        }
        public void RunDownloadTiles(TileColumn tileColumn)
        {
            this.RunDownloadTiles(tileColumn.x, tileColumn.y, tileColumn.baseZoom, tileColumn.maxZoom);
        }


        /// <summary>
        /// Get list of tile coordinates for given bounding box (defined by 2 points - start and end) and given zoom levels
        /// </summary>
        /// <param name="start">Starting point of bounding box. X contains longitude, Y contains latitude.</param>
        /// <param name="end">Ending point of bounding box. X contains longitude, Y contains latitude.</param>
        /// <param name="startZoom"></param>
        /// <param name="endZoom"></param>
        /// <returns></returns>
        private IList<TileCoordinates> getTileCoordinates(PointF start, PointF end, int startZoom, int endZoom)
        {
            int minZoom = startZoom < endZoom ? startZoom : endZoom;
            int maxZoom = startZoom < endZoom ? endZoom : startZoom;

            float top = start.Y > end.Y ? start.Y : end.Y;
            float bottom = start.Y > end.Y ? end.Y : start.Y;

            float left = start.X < end.X ? start.X : end.X;
            float right = start.X < end.X ? end.X : start.X;


            List<TileCoordinates> tiles = new List<TileCoordinates>();
            for (int zoom = minZoom; zoom <= maxZoom; zoom++)
            {
                TileCoordinates tStart = new TileCoordinates(top, left, zoom);
                TileCoordinates tEnd = new TileCoordinates(bottom, right, zoom);

                for (int x = tStart.X; x <= tEnd.X; x++)
                {
                    for (int y = tStart.Y; y <= tEnd.Y; y++)
                    {
                        tiles.Add(new TileCoordinates(x, y, zoom));
                    }
                }
            }
            return tiles;
        }


        private bool downloadTile(TileCoordinates tc, bool overwrite = false)
        {
            if (!this.tileDestination.Exists(tc.X, tc.Y, tc.Zoom) || overwrite)
            {
                try
                {
                    using (Stream tileStream = this.tileSource.GetTileStream(tc.X, tc.Y, tc.Zoom))
                    {
                        this.tileDestination.StoreTile(tileStream, tc.X, tc.Y, tc.Zoom);
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;

        }

        /// <summary>
        /// Download all tiles in given bounding box and zoom levels
        /// </summary>
        /// <param name="start">Starting point of bounding box. X contains longitude, Y contains latitude.</param>
        /// <param name="end">Ending point of bounding box. X contains longitude, Y contains latitude.</param>
        /// <param name="minZoom"></param>
        /// <param name="maxZoom"></param>
        public void RunDownload(PointF start, PointF end, int minZoom, int maxZoom)
        {
            

            IList<TileCoordinates> tiles = this.getTileCoordinates(start, end, minZoom, maxZoom);
            this.downloadedCount = 0;
            this.maxCount = tiles.Count;

            Stopwatch s = new Stopwatch();
            long estimated;
            TimeSpan t;

            int lastZoom = minZoom;
            foreach (TileCoordinates tc in tiles)
            {
                estimated = (this.maxCount - this.downloadedCount) * s.ElapsedMilliseconds;
                s.Reset();
                s.Start();

                this.downloadedCount++;
                
                if (lastZoom != tc.Zoom) Console.WriteLine();
                lastZoom = tc.Zoom;

                
                Console.Write("{0}/{1} {2} ", this.downloadedCount, this.maxCount, tc);

                // download and save tile only if it does not exists already
                if (downloadTile(tc) == false)
                {
                    // just continue 
                    Console.WriteLine("not found");
                    continue;
                }

                t = TimeSpan.FromMilliseconds(estimated);
                string time = string.Format("{0:D2}d {1:D2}h:{2:D2}m:{3:D2}s:{4:D3}ms", t.Days, t.Hours, t.Minutes, t.Seconds, t.Milliseconds);
                Console.WriteLine("done " + time);
                s.Stop();
            }

        }
        
        

        /// <summary>
        /// Check if downloading is running
        /// </summary>
        /// <returns>true if there is at least one thred is downloading, else false</returns>
        public bool IsRunning
        {
            get 
            {
                foreach (Thread t in this.threads)
                {
                    if (t != null && t.IsAlive) return true;
                }
                return false;
            }
            
        }

        /// <summary>
        /// Wait for all downloading threads to finish
        /// </summary>
        public void WaitForDownload()
        {
            foreach (Thread t in this.threads) t.Join();
        }


        private void recurseDownload(int x, int y, int zoom, int maxZoom)
        {
            try 
            {

                // download and save tile only if it does not exists already
                if (!this.tileDestination.Exists(x, y, zoom))
                {
                    using (Stream tileStream = this.tileSource.GetTileStream(x, y, zoom))
                    {
                        this.tileDestination.StoreTile(tileStream, x, y, zoom);
                    
                    }
                }

                this.downloadedCount++;
                if (zoom == maxZoom) return;

                // call recursively one level deeper
                recurseDownload(2 * x, 2 * y, zoom + 1, maxZoom);
                recurseDownload(2 * x + 1, 2 * y, zoom + 1, maxZoom);
                recurseDownload(2 * x, 2 * y + 1, zoom + 1, maxZoom);
                recurseDownload(2 * x + 1, 2 * y + 1, zoom + 1, maxZoom);

            }
            catch (Exception e) 
            {
                String errMessage = String.Format("Error getting tile (x: {0}, y: {1}, zoom: {2}): {3}", x, y, zoom, e.Message);
                //Console.Error.WriteLine(errMessage);
            }
            
        }

        public static int TileCount(int zoom, int maxZoom)
        {
            int diff = (maxZoom + 1) - zoom;
            return (int)((1.0 - Math.Pow((double)4, (double)diff)) / (1.0 - 4.0));

        }

    }
}
