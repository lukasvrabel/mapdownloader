﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MapDownloader
{
    /// <summary>
    /// Holds quad-tree boundaries for Tiles
    /// </summary>
    class TileColumn
    {
        public int x, y, baseZoom, maxZoom;


        public TileColumn(int x, int y, int baseZoom, int maxZoom)
        {
            this.x = x;
            this.y = y;
            this.baseZoom = baseZoom;
            this.maxZoom = maxZoom;
        }


        public bool IsInside(int x, int y, int zoom) {
            if (zoom > maxZoom || zoom < baseZoom) return false;

            int zDiff = zoom - baseZoom;

            while(zoom>baseZoom)
            {
                x /= 2;
                y /= 2;
                zoom --;
            }

            return (this.x == x && this.y == y);

        }
    }
}
