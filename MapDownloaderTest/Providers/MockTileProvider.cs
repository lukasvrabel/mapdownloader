﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MapDownloader;
using System.IO;
using System.Drawing;
using MapDownloaderTest.Properties;
using System.Drawing.Imaging;

namespace MapDownloaderTest.Providers
{
    /// <summary>
    /// Mocking provider, provider stream of given bitmap in jpeg format.
    /// </summary>
    public class MockTileProvider : ITileProvider
    {
        Bitmap bitmap;
        ImageFormat format;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bitmap">bitmap to provide</param>
        public MockTileProvider(Bitmap bitmap, ImageFormat format)
        {
            this.bitmap = bitmap;
            this.format = format;
        }

        /// <summary>
        /// Init with default resource file and format
        /// </summary>
        public MockTileProvider() : this(Resources.tile_0_0_0, ImageFormat.Jpeg) { }
        public MockTileProvider(Bitmap bitmap) : this(bitmap, ImageFormat.Jpeg) { }
        public MockTileProvider(ImageFormat format) : this(Resources.tile_0_0_0, format) { }

        /// <summary>
        /// Returns jpeg stream of given bitmap. Params x, y, zoom not used, they are defined only to satisfy the interface.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public Stream GetTileStream(int x, int y, int zoom)
        {
            return this.GetTileStream();
        }

        public Stream GetTileStream()
        {
            MemoryStream stream = new MemoryStream();
            this.bitmap.Save(stream, this.format);
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Compares the given stream with jpeg stream of bitmap stored in this mock provider.
        /// </summary>
        /// <param name="inputStream"></param>
        public bool IsEqal(Stream inputStream)
        {
            using (MemoryStream storedStream = new MemoryStream())
            {
                this.bitmap.Save(storedStream, this.format);
                return StreamEquals(inputStream, storedStream);
            }
        }

        /// <summary>
        /// Returns true when both streams contain the same data.
        /// </summary>
        /// <param name="stream1"></param>
        /// <param name="stream2"></param>
        /// <returns></returns>
        private static bool StreamEquals(Stream stream1, Stream stream2)
        {
            const int bufferSize = 2048;
            byte[] buffer1 = new byte[bufferSize]; //buffer size
            byte[] buffer2 = new byte[bufferSize];
            while (true)
            {
                int count1 = stream1.Read(buffer1, 0, bufferSize);
                int count2 = stream2.Read(buffer2, 0, bufferSize);

                if (count1 != count2)
                    return false;

                if (count1 == 0)
                    return true;

                // You might replace the following with an efficient "memcmp"
                if (!buffer1.Take(count1).SequenceEqual(buffer2.Take(count2)))
                    return false;
            }
        }
    }
}
