﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MapDownloader
{

    // point in polygon: http://stackoverflow.com/a/16391873/3531606

    class BoundingBoxEnumerator: ITileEnumerator
    {
        //private RectangleF bounds;
        private List<TileCoordinates> coordinates;

        public BoundingBoxEnumerator()
        {

        }

        public IEnumerable<TileCoordinates> Coordinates { get { return this.coordinates; } }

        public int Count { get { return this.coordinates.Count; } }

        public void init(PointF start, PointF end, int startZoom, int endZoom)
        {
            int minZoom = startZoom < endZoom ? startZoom : endZoom;
            int maxZoom = startZoom < endZoom ? endZoom : startZoom;

            float top = start.Y > end.Y ? start.Y : end.Y;
            float bottom = start.Y > end.Y ? end.Y : start.Y;

            float left = start.X < end.X ? start.X : end.X;
            float right = start.X < end.X ? end.X : start.X;


            this.coordinates = new List<TileCoordinates>();
            for (int zoom = minZoom; zoom <= maxZoom; zoom++)
            {
                TileCoordinates tStart = new TileCoordinates(top, left, zoom);
                TileCoordinates tEnd = new TileCoordinates(bottom, right, zoom);

                for (int x = tStart.X; x <= tEnd.X; x++)
                {
                    for (int y = tStart.Y; y <= tEnd.Y; y++)
                    {
                        this.coordinates.Add(new TileCoordinates(x, y, zoom));
                    }
                }
            }
            //return tiles;
        }

        private TileCoordinates get(int index) 
        {

            return null;
        }


    }
}
