﻿using System;
using System.IO;
using System.Net;

namespace MapDownloader.Providers
{
    public class HttpTileProvider: ITileProvider 
    {
        private TileTemplate template;
        private String provider;
        private int zoomOffset;

        public String Provider { get { return this.provider; } set { this.provider = value; } }


        /// <summary>
        /// Provides tiles from web server
        /// </summary>
        /// <param name="urlTemplate">template string used for creating url for individual tiles. Should contain {x} {y} {zoom} {provider}.
        /// Example:<code>http://{provider}.tile.openstreetmap.org/{zoom}/{x}/{y}.png</code>
        /// </param>
        /// <param name="provider">id of map provider</param>
        public HttpTileProvider(String urlTemplate, String provider = "", int zoomOffset = 0)
        {
            this.template = new TileTemplate(urlTemplate, "{x}", "{y}", "{zoom}", "{provider}");
            this.provider = provider;
            this.zoomOffset = zoomOffset;
        }



        /// <summary>
        /// get stream for tile image data
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        /// <exception cref="WebException">the tile url is unavailable</exception>
        /// <returns></returns>
        public Stream GetTileStream(int x, int y, int zoom)
        {
            String url = this.template.GetString(x, y, zoom + this.zoomOffset, this.provider);

            // get tile stream from web
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream stream = httpWebReponse.GetResponseStream();

            return stream;
        }

        public override string ToString() {
            return template.Template;
        }

    }
}