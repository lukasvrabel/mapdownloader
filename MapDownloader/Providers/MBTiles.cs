﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Collections.Specialized;
using System.Data.SQLite;

namespace MapDownloader.Providers
{
    public class MBTiles : ITileProvider, ITileWriter
    {
        private SQLiteDatabase mbtiles;
        private bool invert;
        private string fileName;
        
        public MBTiles(string fileName, bool invert = true, string name="Gina Map Tiles", string format="png" , string version="1.0.0", string description = "Gina Map Tiles", string type = "baselayer", bool overwrite = false)
        {
            if (format != "png" && format != "jpg" ) throw new ArgumentException("format should be 'png' or 'jpg'");
            if (type != "baselayer" && type != "overlay") throw new ArgumentException("type should be 'baselayer' or 'overlay'");
            //if (!overwrite && File.Exists(fileName)) throw new ArgumentException("File " + fileName + " already exists");

            this.fileName = fileName;
            this.mbtiles = new SQLiteDatabase(fileName);
            this.invert = invert;
            if ( ! File.Exists(fileName) || overwrite)
            {
                createNewDB(name, format, version, description, type);    
            }
            
            
        }

        private void createNewDB(string name = "Gina Map Tiles", string format = "png", string version = "1.0.0", string description = "Gina Map Tiles", string type = "baselayer")
        {
            if (format != "png" && format != "jpg") throw new ArgumentException("format should be 'png' or 'jpg'");
            if (type != "baselayer" && type != "overlay") throw new ArgumentException("type should be 'baselayer' or 'overlay'");

            string sql;

            // drop all tables in case some exists
            sql = "DROP TABLE IF EXISTS tiles;";
            mbtiles.ExecuteNonQuery(sql);
            sql = "DROP TABLE IF EXISTS metadata;";
            mbtiles.ExecuteNonQuery(sql);

            // create tile table
            sql = "CREATE TABLE tiles (zoom_level integer, tile_column integer, tile_row integer, tile_data blob);";
            mbtiles.ExecuteNonQuery(sql);
            sql = "CREATE UNIQUE INDEX tiles_index ON tiles (zoom_level, tile_column, tile_row);";
            mbtiles.ExecuteNonQuery(sql);

            // create metadata table
            sql = "CREATE TABLE metadata (name text, value text);";
            mbtiles.ExecuteNonQuery(sql);

            Dictionary<string, string> metadata = new Dictionary<string, string>();
            metadata["name"] = name;
            metadata["format"] = format;
            metadata["version"] = version;
            metadata["description"] = description;
            metadata["type"] = type;

            // insert each metada value into table
            foreach (KeyValuePair<string, string> kv in metadata)
            {
                Dictionary<string, string> row = new Dictionary<string, string>();
                row["name"] = kv.Key;
                row["value"] = kv.Value;
                mbtiles.Insert("metadata", row);
            }

        }

        private int getColumnCoordinate(int x, int zoom)
        {
            return x;
        }

        private int getRowCoordinate(int y, int zoom)
        {
            int tmsRow = y;
            if (this.invert)
            {
                int nRows = (1 << zoom); // Num rows = 2^level
                tmsRow = nRows - 1 - y;
            }
            
            return tmsRow;
        }


        public Stream GetTileStream(int x, int y, int zoom)
        {
            int column = this.getColumnCoordinate(x, zoom);
            int row = this.getRowCoordinate(y, zoom);

            string sql = String.Format("SELECT tile_data FROM tiles WHERE tile_column={0} AND tile_row={1} AND zoom_level={2};", column, row, zoom);
            DataTable dt;
            dt = mbtiles.GetDataTable(sql);
            return new MemoryStream((Byte[])(dt.Rows[0].ItemArray[0]));
        }

        public bool Exists(int x, int y, int zoom)
        {
            int column = this.getColumnCoordinate(x, zoom);
            int row = this.getRowCoordinate(y, zoom);

            string sql = String.Format("SELECT 1 FROM tiles WHERE tile_column={0} AND tile_row={1} AND zoom_level={2};", column, row, zoom);
            string res = mbtiles.ExecuteScalar(sql);
            // if res is empty string, then return false
            return (res != "");
        }

        /// <summary>
        /// Returns tile count
        /// </summary>
        public int TileCount
        {
            get
            {
                string sql = "SELECT COUNT(1) FROM tiles";
                string res = mbtiles.ExecuteScalar(sql);
                int count = int.Parse(res);
                return count;
            }
        }

        public long FileSize
        {
            get
            {
                return new FileInfo(this.fileName).Length;
            }
        }

        public void StoreTile(Stream tileStream, int x, int y, int zoom)
        {
            // read tile data from stream
            const int EXPECTED_LENGTH = 32 * 1024;

            MemoryStream ms = new MemoryStream(EXPECTED_LENGTH);
            byte[] buffer = new byte[EXPECTED_LENGTH];
            int bytesRead;
            while ((bytesRead = tileStream.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, bytesRead);
            }
            byte[] tileData = ms.ToArray();

            // save tile data to sqlite
            int column = this.getColumnCoordinate(x, zoom);
            int row = this.getRowCoordinate(y, zoom);

            string sql = String.Format("INSERT INTO tiles (zoom_level, tile_column, tile_row, tile_data) VALUES ({0}, {1}, {2}, @tiledata)", zoom, column, row);
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add("@tiledata", DbType.Binary).Value = tileData;

            int rowsChanged = mbtiles.ExecuteNonQuery(cmd);
        }



    }
}
