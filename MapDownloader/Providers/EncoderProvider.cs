﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing;

namespace MapDownloader.Providers
{
    public class EncoderProvider : ITileProvider 
    {
        private ImageFormat outFormat;
        private ITileProvider provider;

        public EncoderProvider(ITileProvider provider, ImageFormat outFormat)
        {
            this.provider = provider;
            this.outFormat = outFormat;
        }


        public Stream GetTileStream(int x, int y, int zoom)
        {
            Image image = Image.FromStream(this.provider.GetTileStream(x, y, zoom));
            MemoryStream outStream = new MemoryStream();
            image.Save(outStream, this.outFormat);
            
            outStream.Position = 0;
            return outStream;
        }
    }
}
