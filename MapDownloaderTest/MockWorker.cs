﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MapDownloader;
using System.Threading;

namespace MapDownloaderTest
{
    class MockWorker : MapDownloader.ITileDownloadWorker
    {
        public int CallCount = 0;

        public MockWorker() { this.CallCount = 0; }

        public bool Download(TileCoordinates tc)
        {
            Thread.Sleep(10);
            Interlocked.Add(ref this.CallCount, 1);
            return true;
        }

    }
}
