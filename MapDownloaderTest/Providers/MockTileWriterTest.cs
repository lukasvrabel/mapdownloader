﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace MapDownloaderTest.Providers
{
    [TestClass]
    public class MockTileWriterTest
    {
        [TestMethod]
        public void MockTileWriterTestSpec()
        {
            // create provider and get tile
            MockTileWriter writer = new MockTileWriter();
            Assert.IsTrue(writer.CallCount == 0);

            Assert.IsTrue(writer.Exists(0, 0, 0) == writer.ExistsValue);
            writer.ExistsValue = !writer.ExistsValue;
            Assert.IsTrue(writer.Exists(0, 0, 0) == writer.ExistsValue);

            writer.StoreTile(null, 1, 2, 3);
            Assert.IsTrue(writer.CallCount == 1);
            Assert.IsTrue(writer.HaveBeenCalledWith(1, 2, 3));
            Assert.IsFalse(writer.HaveBeenCalledWith(3, 2, 1));

            Stream s = new MemoryStream();
            writer.StoreTile(s, 4, 3, 5);
            Assert.IsTrue(writer.CallCount == 2);
            Assert.IsTrue(writer.HaveBeenCalledWith(s, 4, 3, 5));
            Assert.IsFalse(writer.HaveBeenCalledWith(null, 1, 2, 3));

        }
    }
}
