﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MapDownloader
{
    /// <summary>
    /// Template for generating parametrized strings.
    /// Example: 
    /// <code>
    ///     TileTemplate t = new TileTemplate("{zoom}/{x}/{y}.png", "{x}", "{y}", "{zoom}"); 
    ///     t.getString(0, 1, 20); // returns "20/0/1.png" - {x} is first, {y} second and {zoom} is last (based on constructor)
    /// </code>
    /// </summary>
    class TileTemplate
    {
        // template containing {0}, {1}, etc
        private string template;

        // original template string
        private string originalTemplate;
        public string Template { get { return this.originalTemplate; } }
        
        /// <summary>
        /// Create template string with named parameters. Use
        /// </summary>
        /// <param name="template">Template string. Example: <code>"{zoom}/{x}/{y}.png"</code></param>
        /// <param name="parameters">Strings corresponding to each parameter. Example: <code>"{x}", "{y}", "{zoom}"</code></param>
        public TileTemplate(String template, params String[] parameters)
        {
            this.setTemplate(template, parameters);

        }

        public TileTemplate setTemplate(String template, params String[] parameters)
        {
            this.originalTemplate = template;
            String tmp = template;

            // convert parameter name strings to {0}, {1}, {2}, etc used by String.Format()
            int id = 0;
            foreach (String par in parameters)
            {
                tmp = tmp.Replace(par, "{" + id + "}");
                id++;
            }
            this.template = tmp;
            return this;
        }

        /// <summary>
        /// Return template string with inserted values for each parameter. Parameter order based on order of constructor parameters.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public string GetString(params Object[] values)
        {
            return String.Format(this.template, values);
        }


        
    }
}
