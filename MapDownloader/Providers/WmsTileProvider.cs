﻿using System;
using System.IO;
using System.Net;
using System.Drawing;
using Gina.Coordinates;

namespace MapDownloader.Providers
{
    /// <summary>
    /// Provides map tiles from WMS Server
    /// </summary>
    public class WmsTileProvider: ITileProvider 
    {
        /// <summary>
        /// Supported Srs projections 
        /// </summary>
        public class Srs
        {
            private Srs(string urlString) { UrlString = urlString; }
            public string UrlString { get; private set; }

            private static Srs epsg3857 = new Srs("EPSG%3A" + "3857");
            private static Srs epsg4326 = new Srs("EPSG%3A" + "4326");

            public static Srs EPSG4326 { get { return epsg4326; } }
            public static Srs EPSG3857 { get { return epsg3857; } }
        }

        /// <summary>
        /// Supported image types
        /// </summary>
        public class Format
        {
            private Format(string urlString) { UrlString = urlString;}
            public string UrlString { get; private set; }

            private static Format png = new Format("image%2Fpng");
            private static Format png8 = new Format("image%2Fpng8");
            private static Format png24 = new Format("image%2Fpng24");
            private static Format png32 = new Format("image%2Fpng32");
            private static Format jpeg = new Format("image%2Fjpeg");
            private static Format bmp = new Format("image%2Fbmp");

            public static Format PNG { get { return png; } }
            public static Format PNG8 { get { return png8; } }
            public static Format PNG24 { get { return png24; } }
            public static Format JPEG { get { return jpeg; } }
            public static Format BMP { get { return bmp; } }
            
        }

        /// <summary>
        /// Wms config parameters
        /// </summary>
        public class WmsParams
        {
            public string Service = "WMS";
            public string Request = "GetMap";
            public string Version = "1.1.1";
            public string Layers = "0";
		    public string Styles = "";
		    public Format Format = Format.PNG8;
            public Srs Srs = Srs.EPSG3857;
            public bool Transparent = false;
            public int TileHeight = 256;
            public int TileWidth = 256;
            
            public string ToUrl()
            {                
                return String.Format("?&SERVICE={0}&REQUEST={1}&VERSION={2}&LAYERS={3}&STYLES={4}&FORMAT={5}&TRANSPARENT={6}&HEIGHT={7}&WIDTH={8}&SRS={9}", 
                    this.Service, this.Request, this.Version, this.Layers, this.Styles, this.Format.UrlString, this.Transparent, this.TileHeight, this.TileWidth, this.Srs.UrlString);
            }
        }


        public WmsParams Parameters { get; set; }
        private string baseUrl;

        /// <summary>
        /// Provides tiles from wms server
        /// </summary>
        
        public WmsTileProvider(string url, WmsParams par)
        {
            this.Parameters = par;
            this.baseUrl = url.Trim();
        }

        public WmsTileProvider(string url, string layers)
        {
            this.Parameters = new WmsParams();
            this.Parameters.Layers = layers;
            this.baseUrl = url.Trim();
        }

        public WmsTileProvider(string url) : this(url, new WmsParams()) { }

        

        /// <summary>
        /// Generate wms url of bounded area
        /// </summary>
        /// <param name="start">top left (north-west) point of bounding box</param>
        /// <param name="end">bottom right (south-east) point of bounding box</param>
        /// <returns></returns>
        private string getUrlFromBounds(GeoPoint start, GeoPoint end)
        {
            double sX, eX;
            double sY, eY;
            start.ToMercator(out sX, out sY);
            end.ToMercator(out eX, out eY);

            string strBBox = String.Format("&BBOX={0},{1},{2},{3}", sX, sY, eX, eY);
            string url = this.baseUrl + this.Parameters.ToUrl() + strBBox;
            return url;
        }

        /// <summary>
        /// Get url for givet tile coordinates
        /// </summary>
        /// <param name="tc">Tile coordinates we want to get url for</param>
        /// <returns></returns>
        private string getUrl(TileCoordinates tc)
        {
            string bBox;
            string bBoxTemplate = "&BBOX={0},{1},{2},{3}";
            TileCoordinates.Bounds bounds;

            Srs currentSrs = this.Parameters.Srs;
            if (currentSrs == Srs.EPSG3857) {
                bounds = tc.Meters;    
            } else if (currentSrs == Srs.EPSG4326) {
                bounds = tc.LatLng;
            } else {
                throw new NotSupportedException("SRS " + currentSrs.ToString() + " not supported");
            }

            bBox = String.Format(bBoxTemplate, bounds.West, bounds.South, bounds.East, bounds.North);
            string url = this.baseUrl + this.Parameters.ToUrl() + bBox;
            return url;
        }

        /// <summary>
        /// get stream for tile image data
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="zoom"></param>
        /// <exception cref="WebException">the tile url is unavailable</exception>
        /// <returns></returns>
        public Stream GetTileStream(int x, int y, int zoom)
        {
            TileCoordinates tc = new TileCoordinates(x, y, zoom);
            string url = this.getUrl(tc);

            //Console.WriteLine(url);
            // get tile stream from web
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream stream = httpWebReponse.GetResponseStream();

            return stream;
        }

        public override string ToString() {
            return this.baseUrl + " " + this.Parameters.ToUrl();
        }

    }
}