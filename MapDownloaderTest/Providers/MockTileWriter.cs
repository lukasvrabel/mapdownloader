﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MapDownloader;

namespace MapDownloaderTest.Providers
{
    class MockTileWriter : ITileWriter
    {

        public MockTileWriter(bool existsValue = false)
        {
            this.ExistsValue = existsValue;
        }

        public bool ExistsValue { get; set; }
        public int CallCount = 0;
        public int CalledWithX, CalledWithY, CalledWithZoom;
        public Stream CalledWithStream;

        public bool HaveBeenCalledWith(int x, int y, int zoom)
        {
            return x == this.CalledWithX && y == this.CalledWithY && zoom == this.CalledWithZoom;
        }
        public bool HaveBeenCalledWith(Stream stream, int x, int y, int zoom)
        {
            return stream == this.CalledWithStream && this.HaveBeenCalledWith(x, y, zoom);
        }

        public bool Exists(int x, int y, int zoom)
        {
            return this.ExistsValue;
        }

        public void StoreTile(Stream tileStream, int x, int y, int zoom)
        {
            this.CallCount++;
            this.CalledWithStream = tileStream;
            this.CalledWithX = x;
            this.CalledWithY = y;
            this.CalledWithZoom = zoom;
        }


        
    }
}
