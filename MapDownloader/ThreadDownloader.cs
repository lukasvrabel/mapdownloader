﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections;
using System.Drawing;
using System.IO;

namespace MapDownloader
{

    

    public class ThreadDownloaderWorker: ITileDownloadWorker
    {
        private ITileProvider provider;
        private ITileWriter writer;

        public ThreadDownloaderWorker(ITileProvider provider, ITileWriter writer)
        {
            this.provider = provider;
            this.writer = writer;
        }

        public void ThreadPoolWrapper(Object context)
        {
            TileCoordinates tc = (TileCoordinates)context;
            Console.WriteLine("callback called for " + tc);
            this.Download(tc);
        }

        public bool Download(TileCoordinates tc)
        {
            bool overwrite = false;

            if (!this.writer.Exists(tc.X, tc.Y, tc.Zoom) || overwrite)
            {
                try
                {
                    using (Stream tileStream = this.provider.GetTileStream(tc.X, tc.Y, tc.Zoom))
                    {
                        this.writer.StoreTile(tileStream, tc.X, tc.Y, tc.Zoom);
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }
    }

    

    public class ThreadDownloader
    {
        private int tilesMax;
        private int tilesProcessed;
        private bool running = false;
        private ManualResetEvent endEvent;
        
        public delegate void DownloadedEventHandler(int num, int max, TileCoordinates coordinates);
        public event DownloadedEventHandler TileDownloadedEvent;

        private void fireEvent(int num, TileCoordinates tc)
        {
            if (TileDownloadedEvent != null)
            {
                TileDownloadedEvent(num, this.tilesMax, tc);
            }
        }

        public ThreadDownloader() { }

        public ThreadDownloader(int maxThreads)
        {
            ThreadPool.SetMaxThreads(maxThreads, maxThreads);
        }

        private void endRun() {
            this.running = false;
            this.endEvent.Set();
            
        }
        public void Run(ITileEnumerator enumerator, ITileDownloadWorker worker)
        {
            if (this.running) throw new InvalidOperationException("Cannot run when already running");

            this.tilesMax = enumerator.Count;
            this.tilesProcessed = 0;
            this.running = true;
            this.endEvent = new ManualResetEvent(false);

            WaitCallback wrapper = delegate(Object context)
            {
                TileCoordinates tc = (TileCoordinates)context;
                bool ret = worker.Download(tc);
                int newProcessedCount = Interlocked.Add(ref this.tilesProcessed, 1);
                
                this.fireEvent(newProcessedCount, tc);

                if (newProcessedCount >= this.tilesMax)
                {
                    this.endRun();
                }
            };

            Console.WriteLine("start work");
            foreach(TileCoordinates tc in enumerator.Coordinates) {
                Console.WriteLine("adding " + tc);
                ThreadPool.QueueUserWorkItem(wrapper, tc);
            }
            
            Console.WriteLine("all end");
        }

        public void RunSingle(ITileEnumerator enumerator, ITileDownloadWorker worker)
        {
            this.tilesMax = enumerator.Count;
            this.tilesProcessed = 0;
            this.running = true;
            this.endEvent = new ManualResetEvent(false);


            foreach (TileCoordinates tc in enumerator.Coordinates)
            {
                worker.Download(tc);

                int newProcessedCount = Interlocked.Add(ref this.tilesProcessed, 1);

                this.fireEvent(newProcessedCount, tc);

                if (newProcessedCount >= this.tilesMax)
                {
                    this.endRun();
                }
            }
            
        }

        public void WaitForEnd()
        {
            //if (! this.running) throw new InvalidOperationException("Cannot wait for end when not running");
            this.endEvent.WaitOne();
        }

      
    }
}
