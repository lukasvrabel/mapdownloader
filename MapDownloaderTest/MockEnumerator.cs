﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MapDownloader;

namespace MapDownloaderTest
{
    class MockEnumerator: MapDownloader.ITileEnumerator
    {
        List<TileCoordinates> coordinates;
        public MockEnumerator(int count) {
            this.coordinates = new List<TileCoordinates>();
            for(int i = 0; i < count; i++) {
                this.coordinates.Add(new TileCoordinates(0, 0, i));
            }
        }


        public IEnumerable<TileCoordinates> Coordinates { get { return this.coordinates; } }

        public int Count { get { return this.coordinates.Count; } }
    }
}
