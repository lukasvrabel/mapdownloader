﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing.Imaging;

namespace MapDownloaderTest.Providers
{
    class Utils
    {
        public static bool checkStreamHeader(Stream stream, ImageFormat format)
        {
            // png header to check if we got png
            byte[] pngHead = new byte[] { 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a };
            
            // jpeg headers - can be JFIF or EXIF
            byte[] jfifHead = new byte[] { 0xff, 0xd8, 0xff, 0xe0 };
            byte[] exifHead = new byte[] { 0xff, 0xd8, 0xff, 0xe1 };

            int maxLength;

            if (format == ImageFormat.Png)
            {
                maxLength = pngHead.Length;
            }
            else if (format == ImageFormat.Jpeg)
            {
                maxLength = jfifHead.Length;
            }
            else
            {
                throw new NotSupportedException("Format " + format.ToString() + " not supported.");
            }

            byte[] streamHead = new byte[maxLength];
            // read first bytes from stream in order to check for png header
            int numBytesToRead = maxLength;
            int numBytesRead = 0;
            do
            {
                // Read may return anything from 0 to numBytesToRead. 
                int n = stream.Read(streamHead, numBytesRead, numBytesToRead);
                numBytesRead += n;
                numBytesToRead -= n;
            } while (numBytesToRead > 0);




            if (format == ImageFormat.Png)
            {
                return streamHead.SequenceEqual(pngHead);
            }
            else if (format == ImageFormat.Jpeg)
            {
                return streamHead.SequenceEqual(jfifHead) || streamHead.SequenceEqual(exifHead);
            }
            else
            {
                throw new NotSupportedException("Format " + format.ToString() + " not supported.");
            }

        }
    }
}
