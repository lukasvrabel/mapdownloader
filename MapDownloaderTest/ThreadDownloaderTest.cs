﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MapDownloader;
using System.Threading;

namespace MapDownloaderTest
{
    [TestClass]
    public class ThreadDownloaderTest
    {

        [TestMethod]
        public void ThreadDownloaderSpec()
        {    
            const int TILE_COUNT = 10;
            ThreadDownloader td = new ThreadDownloader();
            MockEnumerator enumerator = new MockEnumerator(TILE_COUNT);
            MockWorker worker = new MockWorker();
            int eventCount = 0;
            
            ManualResetEvent e = new ManualResetEvent(false);
            td.TileDownloadedEvent += delegate(int num, int max, TileCoordinates tc)
            {
                Interlocked.Add(ref eventCount, 1);
            };
            
            td.Run(enumerator, worker);
            td.WaitForEnd();

            Assert.IsTrue(worker.CallCount == TILE_COUNT, "Worker was not called sufficient number of times");
            Assert.IsTrue(eventCount == TILE_COUNT, "Event was not fired sufficient number of times");
        }
    }
}

