﻿using MapDownloader;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using MapDownloaderTest.Providers;
using System.IO;
using System.Drawing.Imaging;

namespace MapDownloaderTest
{
    
    
    [TestClass]
    public class ThreadDownloaderWorkerTest
    {

        [TestMethod]
        public void Download()
        {
            MockTileProvider provider = new MockTileProvider(ImageFormat.Jpeg);
            MockTileWriter writer = new MockTileWriter(false);
            
            ThreadDownloaderWorker tdw = new ThreadDownloaderWorker(provider, writer);
            TileCoordinates tc = new TileCoordinates(1, 2, 3);

            tdw.Download(tc);

            Assert.IsTrue(writer.CallCount == 1, "writer have been called once");
            Assert.IsTrue(writer.HaveBeenCalledWith(1, 2, 3), "writer have been called with correct coordinates");

            
        }

        [TestMethod]
        public void Exists()
        {
            MockTileProvider provider = new MockTileProvider(ImageFormat.Jpeg);
            MockTileWriter writer = new MockTileWriter();

            ThreadDownloaderWorker tdw = new ThreadDownloaderWorker(provider, writer);
            TileCoordinates tc = new TileCoordinates(1, 2, 3);

            writer.ExistsValue = true;
            tdw.Download(tc);
            Assert.IsTrue(writer.CallCount == 0, "writer not called");
            Assert.IsFalse(writer.HaveBeenCalledWith(1, 2, 3));

        }

        [TestMethod]
        public void Wrapper()
        {
            MockTileProvider provider = new MockTileProvider(ImageFormat.Jpeg);
            MockTileWriter writer = new MockTileWriter(false);

            ThreadDownloaderWorker tdw = new ThreadDownloaderWorker(provider, writer);
            TileCoordinates tc = new TileCoordinates(1, 2, 3);

            tdw.ThreadPoolWrapper(tc);
            Assert.IsTrue(writer.CallCount == 1, "writer have been called once");
            Assert.IsTrue(writer.HaveBeenCalledWith(1, 2, 3), "writer have been called with correct coordinates");
        }
    }
}
