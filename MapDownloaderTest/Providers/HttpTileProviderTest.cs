﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using MapDownloader.Providers;
using System.Drawing.Imaging;

namespace MapDownloaderTest.Providers
{
    [TestClass]
    public class HttpTileProviderTest
    {
        [TestMethod]
        public void HttpTileProviderSpec()
        {
            // create provider and get tile
            HttpTileProvider prov = new HttpTileProvider("http://a.tile.openstreetmap.org/{zoom}/{x}/{y}.png");
            Stream tile = prov.GetTileStream(0, 0, 0);

            Assert.IsTrue(Utils.checkStreamHeader(tile, ImageFormat.Png));


        }
    }
}
