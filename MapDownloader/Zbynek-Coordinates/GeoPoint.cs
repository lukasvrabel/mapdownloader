﻿using System;
using System.Collections.Generic;
using System.Text;
using Gina.Coordinates;
using System.Globalization;
//using Gina.Coders;

namespace Gina.Coordinates
{
    public struct GeoPoint
    {
        public static readonly GeoPoint Empty = new GeoPoint(double.NaN, double.NaN);

        private const double maxLongitude = 177;
        private const double maxLatitude = 85.05112878;

        public double Longitude;
        public double Latitude;
        public bool IsEmpty { get { return this.Equals(Empty); } }


        public GeoPoint(double longitude, double latitude)
        {
            this.Longitude = Math.Min(Math.Max(longitude, -maxLongitude), maxLongitude);
            this.Latitude = Math.Min(Math.Max(latitude, -maxLatitude), maxLatitude);
        }


        /// <summary>
        /// Prevede bod na textovy retezec
        /// </summary>
        /// <returns></returns>
        /*
        public override string ToString()
        {
            return this.IsEmpty ? String.Empty : GeoPointsCoder.Encode(Longitude, Latitude);
        }
         */ 


        /// <summary>
        /// Converts the GeoPoint to the EPGS:3857 coordinates
        /// </summary>
        public void ToMercator(out double x, out double y)
        {
            var num = this.Longitude * 0.017453292519943295;
            x = 6378137.0 * num;
            var a = this.Latitude * 0.017453292519943295;
            y = 3189068.5 * Math.Log((1.0 + Math.Sin(a)) / (1.0 - Math.Sin(a)));
        }


        /// <summary>
        /// Prevede geo-souradnice na linearni
        /// </summary>
        public MapPoint ToMapPoint(int zoom)
        {
            // ziskani linearnich souradnic
            double sinLatitude = Math.Sin(this.Latitude * Math.PI / 180);
            double x = (this.Longitude + 180) / 360;
            double y = 0.5 - Math.Log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI);

            int size = (1 << zoom) * MapTileInfo.TileSize; // (1 << zoom) je celkovy pocet poli
            return new MapPoint((int)(Math.Min(Math.Max(x * size + 0.5, 0), size - 1)), (int)(Math.Min(Math.Max(y * size + 0.5, 0), size - 1)), zoom);
        }


        /// <summary>
        /// Vrati linearni souradnice leveho horniho rohu
        /// </summary>
        /// <returns></returns>
        public static GeoPoint FromMapPoint(MapPoint point)
        {
            double size = (1L << point.Zoom) * MapTileInfo.TileSize;
            double x = point.X / size - 0.5;
            double y = point.Y / size - 0.5;

            return new GeoPoint(360 * x, 90 - 360 * Math.Atan(Math.Exp(y * 2 * Math.PI)) / Math.PI);
        }


        /// <summary>
        /// Converts the specified string representation of a geographical point to its GeoPoint equivalent
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        /*
        public static GeoPoint Parse(string s)
        {
            return String.IsNullOrEmpty(s) ? GeoPoint.Empty : GeoPointsCoder.DecodeSingle(s);
        }
         */


        /// <summary>
        /// Offsets the position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Offset(double lon, double lat)
        {
            this.Longitude += lon;
            this.Latitude += lat;
        }


        /// <summary>
        /// Gets if the point equals the current point
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            GeoPoint point = (GeoPoint)obj;
            return this.Longitude.Equals(point.Longitude) && this.Latitude.Equals(point.Latitude);
        }


        /// <summary>
        /// Gets the hash code of the object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.Longitude.GetHashCode() ^ this.Latitude.GetHashCode();
        }



        /// <summary>
        /// Returns the value in radians
        /// </summary>
        private double getRadians(double x)
        {
            return x * Math.PI / 180;
        }


        /// <summary>
        /// Returns the distance of two points in kilometers
        /// </summary>
        public double Distance(GeoPoint p)
        {
            double R = 6379; // km
            double dLat = this.getRadians(p.Latitude - this.Latitude);
            double dLon = this.getRadians(p.Longitude - this.Longitude);
            double lat1 = this.getRadians(this.Latitude);
            double lat2 = this.getRadians(p.Latitude);

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = R * c;

            return d;
        }


        /// <summary>
        /// Returns the JTSK coordinates
        /// http://www.pecina.cz/krovak.html
        /// </summary>
        public void ToJTSK(out double x, out double y)
        {
            var d2r = Math.PI / 180;
            var a = 6378137.0;
            var f1 = 298.257223563;
            var dx = -570.69;
            var dy = -85.69;
            var dz = -462.84;
            var wx = 4.99821 / 3600 * Math.PI / 180;
            var wy = 1.58676 / 3600 * Math.PI / 180;
            var wz = 5.2611 / 3600 * Math.PI / 180;
            var m = -3.543e-6;

            var B = this.Latitude * d2r;
            var L = this.Longitude * d2r;

            var e2 = 1 - Math.Pow(1 - 1 / f1, 2);
            var rho = a / Math.Sqrt(1 - e2 * Math.Pow(Math.Sin(B), 2));
            var x1 = rho * Math.Cos(B) * Math.Cos(L);
            var y1 = rho * Math.Cos(B) * Math.Sin(L);
            var z1 = (1 - e2) * rho * Math.Sin(B);

            var x2 = dx + (1 + m) * (x1 + wz * y1 - wy * z1);
            var y2 = dy + (1 + m) * (-wz * x1 + y1 + wx * z1);
            var z2 = dz + (1 + m) * (wy * x1 - wx * y1 + z1);

            a = 6377397.15508;
            f1 = 299.152812853;
            var ab = f1 / (f1 - 1);
            var p = Math.Sqrt(Math.Pow(x2, 2) + Math.Pow(y2, 2));
            e2 = 1 - Math.Pow(1 - 1 / f1, 2);
            var th = Math.Atan(z2 * ab / p);
            var st = Math.Sin(th);
            var ct = Math.Cos(th);
            var t = (z2 + e2 * ab * a * (st * st * st)) / (p - e2 * a * (ct * ct * ct));

            B = Math.Atan(t);
            L = 2 * Math.Atan(y2 / (p + x2));

            a = 6377397.15508;
            var e = 0.081696831215303;
            var n = 0.97992470462083;
            var rho0 = 12310230.12797036;
            var sinUQ = 0.863499969506341;
            var cosUQ = 0.504348889819882;
            var sinVQ = 0.420215144586493;
            var cosVQ = 0.907424504992097;
            var alpha = 1.000597498371542;
            var k2 = 1.00685001861538;

            var sinB = Math.Sin(B);
            t = (1 - e * sinB) / (1 + e * sinB);
            t = Math.Pow(1 + sinB, 2) / (1 - Math.Pow(sinB, 2)) * Math.Exp(e * Math.Log(t));
            t = k2 * Math.Exp(alpha * Math.Log(t));

            var sinU = (t - 1) / (t + 1);
            var cosU = Math.Sqrt(1 - sinU * sinU);
            var V = alpha * L;
            var sinV = Math.Sin(V);
            var cosV = Math.Cos(V);
            var cosDV = cosVQ * cosV + sinVQ * sinV;
            var sinDV = sinVQ * cosV - cosVQ * sinV;
            var sinS = sinUQ * sinU + cosUQ * cosU * cosDV;
            var cosS = Math.Sqrt(1 - sinS * sinS);
            var sinD = sinDV * cosU / cosS;
            var cosD = Math.Sqrt(1 - sinD * sinD);

            var eps = n * Math.Atan(sinD / cosD);
            rho = rho0 * Math.Exp(-n * Math.Log((1 + sinS) / cosS));

            x = rho * Math.Sin(eps);
            y = rho * Math.Cos(eps);
        }
    }
}
