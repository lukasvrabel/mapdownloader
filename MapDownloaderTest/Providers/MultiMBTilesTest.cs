﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MapDownloader.Providers;
using System.IO;
using MapDownloaderTest.Properties;
using System.Drawing.Imaging;

namespace MapDownloaderTest.Providers
{
    [TestClass]
    public class MultiMBTilesTest
    {
        [TestMethod]
        public void MultiMBTilesSpec()
        {
            MockTileProvider mock = new MockTileProvider();      

            string baseFileName = "_testfile";

            string fileName1 = baseFileName + "00.mbtiles";
            string fileName2 = baseFileName + "01.mbtiles";

            long twoTilesFileSize = 14000;

            File.Delete(fileName1);
            File.Delete(fileName2);

            // create multi tiles with max tile count per file set to 2
            MultiMBTiles tiles = new MultiMBTiles(baseFileName: baseFileName, maxFileSize: twoTilesFileSize);

            // store three tiles -> it should write tiles to two files
            for (int i = 0; i < 3; i++)
            {
                Assert.IsFalse(tiles.Exists(0, 0, i));
                tiles.StoreTile(mock.GetTileStream(), 0, 0, i);
            }

            long file1Size = new FileInfo(fileName1).Length;
            long file2Size = new FileInfo(fileName2).Length;

            Assert.IsTrue(file1Size > twoTilesFileSize, "first file is not large enough");
            Assert.IsTrue(file2Size < twoTilesFileSize, "second file is too large");
            

            // first file should contain only first two tiles (0,0,0) and (0,0,1)
            MBTiles test = new MBTiles(fileName1);
            
            Assert.IsTrue(test.Exists(0, 0, 0), "first tile is not in first file");
            Assert.IsTrue(test.Exists(0, 0, 1), "second tile is not in first file");
            Assert.IsFalse(test.Exists(0, 0, 2), "third tile is in first file");
            

            Stream storedTile = test.GetTileStream(0, 0, 0);
            Assert.IsTrue(Utils.checkStreamHeader(storedTile, ImageFormat.Jpeg));

            // second file should contain only third tile (0,0,2)
            test = new MBTiles(fileName2);
            Assert.IsFalse(test.Exists(0, 0, 0), "first tile is in seconds file");
            Assert.IsFalse(test.Exists(0, 0, 1), "second tile is in seconds file");
            Assert.IsTrue(test.Exists(0, 0, 2), "third tile is not in seconds file");
            
        }
    }
}
