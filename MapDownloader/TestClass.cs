﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MapDownloader
{
    public struct TestClass
    {
    }

    public class EventsTest
    {
        public delegate void TestEventHandler(string str, int num);
        public event TestEventHandler TestEvent;

        public void FireEvent(string str, int num)
        {
            if (TestEvent != null)
            {
                TestEvent(str, num);
            }
        }
    }

    public class ThreadWorker
    {
        public void ThreadPoolCallback(Object context)
        {
            
            int delay = (int)context;
            Console.WriteLine("callback called for " + delay);
            this.doWork(delay);
        }

        private void doWork(int delay)
        {
            Thread.Sleep(delay * 100);
            Console.WriteLine("Doing work for " + delay);
        }
    }

    public class ThreadTest
    {
        private ThreadWorker worker;

        public ThreadTest() { }

        public ThreadTest(int maxThreads)
        {
            ThreadPool.SetMaxThreads(maxThreads, maxThreads);
        }

        public void doWork(int count)
        {
            worker = new ThreadWorker();
            Console.WriteLine("start work");
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("adding " + i);
                ThreadPool.QueueUserWorkItem(worker.ThreadPoolCallback, i);
            }
            Console.WriteLine("all added");
        }
    }
}
