﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MapDownloader
{
    /// <summary>
    /// Class representing tile coordinates in global spherical mercator. Computed from longitude, latitude.
    /// </summary>
    public class TileCoordinates
    {
        public class Bounds
        {
            public float North;
            public float South;
            public float West;
            public float East;

            public Bounds(PointF start, PointF end)
            {
                this.North = start.Y > end.Y ? start.Y : end.Y;
                this.South = start.Y > end.Y ? end.Y : start.Y;

                this.West = start.X < end.X ? start.X : end.X;
                this.East = start.X < end.X ? end.X : start.X;
            }

            override public string ToString()
            {
                return String.Format("(W{0} N{1} E{2} S{3})", this.West, this.North, this.East, this.South);
            }
        }

        public int X;
        public int Y;
        public int Zoom;

        public Bounds Meters { 
            get
            {
                Bounds pixels = this.Pixels;
                PointF WN = new PointF(pixels.West, pixels.North);
                PointF ES = new PointF(pixels.East, pixels.South);

                PointF start = pixelsToMeters(WN, this.Zoom);
                PointF end = pixelsToMeters(ES, this.Zoom);
                return new Bounds(start, end);
            }
        }

        public Bounds Pixels
        {
            get
            {
                PointF pixels = tileToPixels(this.X, this.Y, this.Zoom);
                PointF end = new PointF(pixels.X + TILE_WIDTH, pixels.Y + TILE_WIDTH);
                return new Bounds(pixels, end);
            }
        }

        public Bounds LatLng { 
            get
            {
                Bounds meters = this.Meters;

                PointF start = metersToLonLat(meters.West, meters.North);
                PointF end = metersToLonLat(meters.East, meters.South);

                return new Bounds(start, end);
            } 
        }
        


        private const int TILE_WIDTH = 256;

        private const float initialResolution = 2 * (float)Math.PI * 6378137 / TILE_WIDTH;
        private const float originShift = 2 * (float)Math.PI * 6378137 / 2.0f;

        public override string ToString()
        {
            return string.Format("{0}:({1}, {2}) {3}", "C", this.X, this.Y, this.Zoom);
        }

        /// <summary>
        /// Resolution (meters/pixel) for given zoom level (measured at Equator)"
        /// </summary>
        /// <param name="zoom"></param>
        /// <returns></returns>
        private static double getResolution(int zoom)
        {
            return initialResolution / Math.Pow(2, zoom);
        }

        private static PointF latLonToMeters(double lat, double lon)
        {
            PointF p = new PointF();
            p.X = (float)(lon * originShift / 180.0);
            p.Y = (float)(Math.Log(Math.Tan((90 + lat) * Math.PI / 360.0)) / (Math.PI / 180.0));
            p.Y = (float)(p.Y * originShift / 180.0);
            return p;
        }

        private static PointF metersToLonLat(float mx, float my)
        {
            double lon = (mx / originShift) * 180;
            double lat = (my / originShift) * 180;
            lat = 180.0 / Math.PI * (2 * Math.Atan( Math.Exp( lat * Math.PI / 180.0 ) ) - Math.PI / 2.0);
            PointF p = new PointF((float)lon, (float)lat);
            return p;
        }


        private static PointF metersToPixels(PointF meters, int zoom)
        {
            double res = initialResolution / Math.Pow(2, zoom);

            PointF p = new PointF();
            p.X = (float)((meters.X + originShift) / res);
            p.Y = (float)((meters.Y + originShift) / res);
            return p;
        }

        
        private static PointF pixelsToMeters(PointF pixels, int zoom)
        {
            double res = initialResolution / Math.Pow(2, zoom);

            double mx = pixels.X * res - originShift;
            double my = pixels.Y * res - originShift;

            PointF p = new PointF((float)mx, (float)my);

            return p;
        }


        private static Point pixelsToTile(PointF pixels, int zoom)
        {
            Point t = new Point();
            t.X = (int)Math.Ceiling(pixels.X / TILE_WIDTH) - 1;
            t.Y = (int)Math.Ceiling(pixels.Y / TILE_WIDTH) - 1;
            // reverse y from TMS 
            t.Y = (int)(Math.Pow(2, zoom) - 1) - t.Y;
            return t;
        }


        private static PointF tileToPixels(int x, int y, int zoom)
        {
            int px = x * TILE_WIDTH;
            // reverse y from TMS 
            y = (int)(Math.Pow(2, zoom) - 1) - y;
            int py = y * TILE_WIDTH;
            return new PointF(px, py);
        }

        public TileCoordinates(int x, int y, int zoom)
        {
            this.X = x;
            this.Y = y;
            this.Zoom = zoom;
        }

        public TileCoordinates(double lat, double lon, int zoom)
        {
            PointF m = latLonToMeters(lat, lon);
            PointF p = metersToPixels(m, zoom);
            Point t = pixelsToTile(p, zoom);
            this.X = t.X;
            this.Y = t.Y;
            this.Zoom = zoom;
        }

    }
}
