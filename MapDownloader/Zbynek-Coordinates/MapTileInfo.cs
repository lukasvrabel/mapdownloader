﻿using System;
using System.Collections.Generic;
using System.Text;
using Gina.Coordinates;

namespace Gina.Coordinates
{
    public struct MapTileInfo
    {
        public const int ParentTileLimit = 4;
        public static int TileSize = 256;

        public int X;
        public int Y;
        public int Zoom;


        public MapTileInfo(int x, int y, int zoom)
        {
            this.X = x;
            this.Y = y;
            this.Zoom = zoom;
        }


        /// <summary>
        /// Normalizes the coordinates
        /// </summary>
        public void Normalize()
        {
            int maximum = 1 << Zoom;
            X %= maximum;
            Y %= maximum;

            if (X < 0)
                X += maximum;

            if (Y < 0)
                Y += maximum;
        }


        /// <summary>
        /// Prevede bod na textovy retezec
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return GetQuadKey(X, Y, Zoom);
        }


        /// <summary>
        /// Converts the specified string representation of a map tile coordinates to its MapTileInfo equivalent
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static MapTileInfo Parse(string quadKey)
        {
            int x = 0, y = 0;
            int shift = quadKey.Length;
            foreach (char q in quadKey)
            {
                x |= (1 & q) << --shift;
                y |= (2 & q) << shift >> 1;
            }
            return new MapTileInfo(x, y, quadKey.Length);
        }


        /// <summary>
        /// Converts the specified string representation of a map tile coordinates to its MapTileInfo equivalent
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static MapTileInfo Parse(char[] quadKey)
        {
            int x = 0, y = 0;
            int shift = quadKey.Length;
            foreach (char q in quadKey)
            {
                x |= (1 & q) << --shift;
                y |= (2 & q) << shift >> 1;
            }
            return new MapTileInfo(x, y, quadKey.Length);
        }


        /// <summary>
        /// Ziskani klice pro urceni nazvu casti mapy
        /// </summary>
        /// <param name="mapPoint"></param>
        /// <returns></returns>
        public static string GetQuadKey(int x, int y, int z)
        {
            decimal result = 0, power = 1;
            for (int i = 0; i < z; power *= 10, i++)
                result += power * ((x >> i & 1) | ((y >> i & 1) << 1));

            return result.ToString().PadLeft(z, '0');
        }
    }
}
