﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Drawing;
using System.Threading;
using System.Xml.Linq;
using System.IO.Compression;
using System.Data;
using System.Data.SQLite;
using Gina.Coordinates;
using MapDownloader.Providers;
using System.Drawing.Imaging;




namespace MapDownloader
{

    
    class Program
    {
        static void LogIt(string str, int num)
        {
            Console.WriteLine("str: " + str);
            Console.WriteLine("num: " + num);
        }

        static void Main(string[] args)
        {
            PointF haitiStart = new PointF(-72.856290f, 20.130451f);
            PointF haitiEnd = new PointF(-71.653288f, 17.978446f);

            //PointF prahaStart = new PointF(14.26f, 50.13f);
            PointF prahaStart = new PointF(13.851528f, 50.392433f);
            //PointF prahaEnd = new PointF(14.6f, 50f);
            PointF prahaEnd = new PointF(13.964653f, 50.330560f);
            
            PointF olomoucStart = new PointF(16.6993192f, 50.46238f);
            PointF olomoucEnd = new PointF(17.9735263f, 49.2819928f);
            
            PointF jihlavaStart = new PointF(15.540433f, 49.434073f);
            PointF jihlavaEnd = new PointF(15.644117f, 49.387388f);


            PointF vysocinaStart = new PointF(14.866708f, 49.871604f);
            PointF vysocinaHalfEnd = new PointF(15.6481105f, 48.923377f);
            PointF vysocinaHalfStart = new PointF(15.6481105f, 49.871604f);
            PointF vysocinaEnd = new PointF(16.429513f, 48.923377f);


            PointF jmkStart = new PointF(15.543342f, 49.632943f);
            PointF jmkHalfEnd = new PointF(16.595075f, 48.615745f);
            PointF jmkHalfStart = new PointF(16.595075f, 49.632943f);
            PointF jmkEnd = new PointF(17.646808f, 48.615745f);

            PointF olStart = new PointF(16.696478f, 50.469734f);
            PointF olEnd = new PointF(17.924042f, 49.266346f);

            PointF pceStart = new PointF(15.362966f, 50.210856f);
            PointF pceEnd = new PointF(16.873451f, 49.569434f);

            PointF ostravaStart = new PointF(17.578125f, 50.176898f);
            PointF ostravaEnd = new PointF(18.89099f, 49.271389f);

            PointF kvStart = new PointF(12.09059f, 50.4603f);
            PointF kvEnd = new PointF(13.3018f, 49.8904f);

            PointF crStart = new PointF(12.090626f, 51.056071f);
            PointF crEnd = new PointF(18.859921f, 48.550706f);



            PointF start = pceStart;
            PointF end = pceEnd;

            // providers and writers setup
            //WmsTileProvider wmsProv = new WmsTileProvider("http://datsklad.izscr.cz/mapservices/services/BaseMaps/GINA_CR_WGS_extend1/MapServer/WMSServer?");
            //WmsTileProvider wmsProv = new WmsTileProvider("http://geoportal.cuzk.cz/WMS_ORTOFOTO_PUB/WMService.aspx?", "GR_ORTFOTORGB");
            WmsTileProvider wmsProv = new WmsTileProvider("http://ags.cuzk.cz/arcgis/services/ortofoto/MapServer/WMSServer?", "0");
        
            wmsProv.Parameters.Format = WmsTileProvider.Format.JPEG;

            HttpTileProvider httpProv = new HttpTileProvider("http://ginasystem.com/maptiles/endpoints/maptilesendpoint.asmx?provider={provider}&z={zoom}&x={x}&y={y}", "0");
            //HttpTileProvider httpProv = new HttpTileProvider("http://172.16.7.10/osm_tiles/{zoom}/{x}/{y}.jpeg");
            
            //EncoderProvider encoder = new EncoderProvider(httpProv, ImageFormat.Jpeg);
            
            //MBTiles mbIn = new MBTiles(fileName: "ol.mbtiles.002", format: "jpg");
            //MBTiles mbOut = new MBTiles(fileName: "kvk.mbtiles", format: "jpg");
            MultiMBTiles mult = new MultiMBTiles("jmk-ortofoto", 1000*1024*1024);

            // create downloader and run
            //TileDownloader down = new TileDownloader(wmsProv, mult);    
            //down.RunDownload(start, end, 10, 18);

            
            EventsTest et = new EventsTest();
            et.TestEvent += LogIt;
            et.FireEvent("test", 1);
            et.FireEvent("asd", 222);

            ThreadDownloader tt = new ThreadDownloader();
            BoundingBoxEnumerator bbEnum = new BoundingBoxEnumerator();
            ThreadDownloaderWorker worker = new ThreadDownloaderWorker(wmsProv, mult);

            bbEnum.init(jmkStart, jmkEnd, 4, 19);

            tt.TileDownloadedEvent += delegate(int num, int max, TileCoordinates tc)
            {
                Console.Write("{0}/{1} {2}\n", num, max, tc);
            };

            //tt.Run(bbEnum, worker);
            tt.RunSingle(bbEnum, worker);
            

            tt.WaitForEnd();
            Console.WriteLine("done");

            
        }
    }
}

