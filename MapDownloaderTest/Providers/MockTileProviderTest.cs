﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Drawing.Imaging;

namespace MapDownloaderTest.Providers
{
    [TestClass]
    public class MockTileProviderTest
    {
        // test not working (endless loop)
        [TestMethod]
        public void MockTileProviderSpec()
        {
            // create provider and get tile
            MockTileProvider prov = new MockTileProvider(ImageFormat.Jpeg);
            Stream tile = prov.GetTileStream(0, 0, 0);
            Assert.IsTrue(Utils.checkStreamHeader(tile, ImageFormat.Jpeg));

            prov = new MockTileProvider(ImageFormat.Png);
            tile = prov.GetTileStream(0, 0, 0);
            Assert.IsTrue(Utils.checkStreamHeader(tile, ImageFormat.Png));
        }
    }
}
