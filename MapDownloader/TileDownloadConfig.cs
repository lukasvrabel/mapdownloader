﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using MapDownloader.Providers;

namespace MapDownloader
{
    /// <summary>
    /// Represents config file for tile downloading
    /// </summary>
    class TileDownloadConfig
    {
        /// <summary>
        /// Simple internal storage class for one tile config entry
        /// </summary>
        private class ConfigEntry
        {
            public TileDownloader downloader;
            public TileColumn tileColumn;
            public string zipFileName;
            public string tempDirectory;

            public ConfigEntry(TileDownloader downloader, TileColumn tileColumn, string zipFileName, string tempDirectory)
            {
                this.downloader = downloader;
                this.tileColumn = tileColumn;
                this.tempDirectory = tempDirectory;
                this.zipFileName = zipFileName;
            }
        }

        private class ProviderEntry
        {
            public ITileProvider provider;
            
            public string type;
            public string fileName;
            public int x, y, zoomStart, zoomMax;


            
        }
        
        List<ConfigEntry> entries;
        public List<TileDownloader> Downloaders { get { return this.entries.Select(entry => entry.downloader).ToList(); } }
        public List<string> TempDirectories { get { return this.entries.Select(entry => entry.tempDirectory).ToList(); } }
        public List<TileColumn> TileColumns { get { return this.entries.Select(entry => entry.tileColumn).ToList(); } }

        public TileDownloadConfig(string configDirectory)
        {
            this.entries = ReadConfig(configDirectory);
        }

        /// <summary>
        /// Start downloading tiles
        /// </summary>
        /// <returns></returns>
        public TileDownloadConfig RunDownload()
        {
            foreach(ConfigEntry entry in this.entries) {
                entry.downloader.RunDownloadTiles(entry.tileColumn);
            }
            return this;
        }
        
        /// <summary>
        /// Zip temp directories
        /// </summary>
        /// <param name="overwriteZips">If true, then existing zip file will be overwritten by new one (not updated!)</param>
        /// <param name="removeDirs">If true, then remove directories after zip</param>
        /// <returns></returns>
        public TileDownloadConfig ZipDirectories(bool overwriteZips = false, bool removeDirs = false)
        {
            /*
            foreach (ConfigEntry entry in this.entries)
            {
                if ( ! File.Exists(entry.zipFileName) )
                {
                    ZipFile.CreateFromDirectory(entry.tempDirectory, entry.zipFileName, CompressionLevel.Optimal, false);
                }
                else if (overwriteZips)
                {
                    File.Delete(entry.zipFileName);
                    ZipFile.CreateFromDirectory(entry.tempDirectory, entry.zipFileName, CompressionLevel.Optimal, false);
                }
                
                if (removeDirs)
                {
                    Directory.Delete(entry.tempDirectory, true);
                }
                    
            }
            */
            return this;
        }


        /// <summary>
        /// True if download is running
        /// </summary>
        public bool IsRunning
        {
            get
            {
                foreach (TileDownloader td in this.Downloaders)
                {
                    if (td.IsRunning) return true;
                }
                return false;
            }
        }

        

        /// <summary>
        /// Read config and generate entries list
        /// </summary>
        /// <param name="configDirectory">directory path containing config file </param>
        private static List<ConfigEntry> ReadConfig(string configDirectory)
        {
            // source provider
            ITileProvider provider;

            // default zip entry path
            string defaultEntryPath;

            List<ConfigEntry> entries = new List<ConfigEntry>();
            
            // parse config and create downloaders for each tiles file
            XElement xmlConfig = XElement.Load("config.xml");

            // get desired entry path to created zip files
            XElement entryPathNode = xmlConfig.Descendants("default-entry-path").FirstOrDefault();
            defaultEntryPath = entryPathNode.Value;


            provider = createProvider(xmlConfig);

            IEnumerable<XElement> tileNodes = xmlConfig.Descendants("tiles");

            foreach (XElement tileNode in tileNodes)
            {
                // read and save temp directory path for zipping
                string fileName = tileNode.Attribute("file").Value;
                string dirName = fileName.Substring(0, fileName.LastIndexOf("."));
                
                // read destination and create downloader
                ITileWriter destination = createTileWriter(tileNode, defaultEntryPath);
                TileDownloader td = new TileDownloader(provider, destination);

                // read tile column and run download
                TileColumn tc = createTileColumn(tileNode);
                //td.RunDownloadTiles(tc);

                entries.Add(new ConfigEntry(td, tc, fileName, dirName));
            }

            return entries;
        }


        /// <summary>
        /// Read config and generate entries list
        /// </summary>
        /// <param name="configDirectory">directory path containing config file </param>
        public static void ReadNewConfig(string configDirectory)
        {

            XPathNavigator nav;
            XPathDocument docNav;
            XPathNodeIterator iter;
            String strExpression;

            List<ConfigEntry> entries = new List<ConfigEntry>();

            docNav = new XPathDocument(@"newconfig.xml");
            nav = docNav.CreateNavigator();

            strExpression = "/gina-tiles-config/tile-sources/defaults/zip-entry-path";
            Console.WriteLine("{0}: {1}", strExpression, nav.SelectSingleNode(strExpression).Value);

            strExpression = "/gina-tiles-config/tile-sources//provider[@type='zip' or @type='mbtiles']";
            iter = nav.Select(strExpression);

            string[] attrs = new string[]{"type", "file-name", "zoom-start", "x", "y", "zoom-max"};

            Dictionary<string, ProviderEntry> providers = new Dictionary<string, ProviderEntry>();

            while(iter.MoveNext())
            {
                
                ProviderEntry entry = new ProviderEntry();

                entry.type = iter.Current.GetAttribute("type", "");
                entry.fileName = iter.Current.GetAttribute("file-name", "");
                entry.zoomStart = int.Parse( iter.Current.GetAttribute("zoom-start", "") );
                entry.x = int.Parse(iter.Current.GetAttribute("x", ""));
                entry.y = int.Parse(iter.Current.GetAttribute("y", "") );
                entry.zoomMax = int.Parse(iter.Current.GetAttribute("zoom-max", "") );

                providers[entry.fileName] = entry;
            }

            Console.WriteLine();
            foreach (KeyValuePair<string, ProviderEntry> entry in providers)
            {
                Console.WriteLine("node: {0}", entry.Key);

                Console.WriteLine("\ttype: {0}", entry.Value.type);
                Console.WriteLine("\tfile-name: {0}", entry.Value.fileName);
                
                Console.WriteLine("\tx: {0}", entry.Value.x);
                Console.WriteLine("\ty: {0}", entry.Value.y);
                Console.WriteLine("\tzoom-start: {0}", entry.Value.zoomStart);
                Console.WriteLine("\tzoom-max: {0}", entry.Value.zoomMax);
            }


            return;


            // source provider
            ITileProvider provider;

            // default zip entry path
            string defaultEntryPath;

            

            // parse config and create downloaders for each tiles file
            XElement xmlConfig = XElement.Load("config.xml");

            // get desired entry path to created zip files
            XElement entryPathNode = xmlConfig.Descendants("default-entry-path").FirstOrDefault();
            defaultEntryPath = entryPathNode.Value;


            provider = createProvider(xmlConfig);

            IEnumerable<XElement> tileNodes = xmlConfig.Descendants("tiles");

            foreach (XElement tileNode in tileNodes)
            {
                // read and save temp directory path for zipping
                string fileName = tileNode.Attribute("file").Value;
                string dirName = fileName.Substring(0, fileName.LastIndexOf("."));

                // read destination and create downloader
                ITileWriter destination = createTileWriter(tileNode, defaultEntryPath);
                TileDownloader td = new TileDownloader(provider, destination);

                // read tile column and run download
                TileColumn tc = createTileColumn(tileNode);
                //td.RunDownloadTiles(tc);

                entries.Add(new ConfigEntry(td, tc, fileName, dirName));
            }

            //return entries;
        }


        /// <summary>
        /// Create tile provider based on config
        /// </summary>
        /// <param name="config">xml element containing config params</param>
        /// <returns></returns>
        private static ITileProvider createProvider(XElement xmlConfigNode)
        {
            ITileProvider provider;

            XElement sourceNode = xmlConfigNode.Descendants("source").FirstOrDefault();

            string sourceType = (string)sourceNode.Attribute("type");
            switch (sourceType)
            {
                case "web":
                    string url = (string)sourceNode.Attribute("url");
                    provider = new HttpTileProvider(url);
                    break;
                case "directory":
                    string dirPath = (string)sourceNode.Attribute("directory-path");
                    string dirEntryPath = (string)sourceNode.Attribute("entry-path");
                    provider = new TileFolder(dirPath, dirEntryPath);
                    break;
                default:
                    throw new Exception("Error: invalid source type \"" + (string)sourceNode.Attribute("type") + "\"");
            }

            return provider;
        }


        /// <summary>
        /// Create tile folder destination for corresponding tile node
        /// </summary>
        /// <param name="tileNode"></param>
        /// <param name="defaultEntryPath"></param>
        /// <returns></returns>
        private static ITileWriter createTileWriter(XElement tileNode, string defaultEntryPath)
        {

            // zip file name
            string fileName = tileNode.Attribute("file").Value;
            // temp dir file name
            string dirName = fileName.Substring(0, fileName.LastIndexOf("."));

            // get entry path from tile if it exists
            string entryPath = (string)tileNode.Attribute("entry-path");
            if (entryPath == null) { entryPath = defaultEntryPath; }
            

            // create destination folder
            TileFolder dest = new TileFolder(dirName, entryPath);

            return dest;
        }


        /// <summary>
        /// Creates tile column based on tile node
        /// </summary>
        /// <param name="tileNode"></param>
        private static TileColumn createTileColumn(XElement tileNode)
        {
            // parse coordinates for TileColumn
            int x = int.Parse(tileNode.Attribute("x").Value),
                y = int.Parse(tileNode.Attribute("y").Value),
                zoom = int.Parse(tileNode.Attribute("zoom").Value),
                maxZoom = int.Parse(tileNode.Attribute("zoom-max").Value);
            TileColumn tc = new TileColumn(x, y, zoom, maxZoom);
            return tc;
        }
    }

}
