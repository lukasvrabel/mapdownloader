﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing.Imaging;
using System.IO;
using MapDownloader;
using MapDownloader.Providers;

namespace MapDownloaderTest.Providers
{
    [TestClass]
    public class EncoderProviderTest
    {
        [TestMethod]
        public void EncoderProviderSpec()
        {
            MockTileProvider mockProv = new MockTileProvider(ImageFormat.Jpeg);

            EncoderProvider prov = new EncoderProvider(mockProv, ImageFormat.Png);
            Stream tileStream = prov.GetTileStream(0, 0, 0);
            Assert.IsTrue(Utils.checkStreamHeader(tileStream, ImageFormat.Png));

            mockProv = new MockTileProvider(ImageFormat.Png);
            prov = new EncoderProvider(mockProv, ImageFormat.Jpeg);
            tileStream = prov.GetTileStream(0, 0, 0);
            Assert.IsTrue(Utils.checkStreamHeader(tileStream, ImageFormat.Jpeg));

        }
    }
}
