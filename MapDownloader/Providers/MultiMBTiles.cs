﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MapDownloader.Providers
{
    /// <summary>
    /// Creates multiple MBTiles files. Each file has up to maxTileCount tiles, then a new file is created. File naming convention for baseFileName="tiles": tiles00.mbtiles, tiles01.mbtiles, tiles02.mbtiles, etc...
    /// </summary>
    public class MultiMBTiles : ITileWriter, ITileProvider
    {
        private struct Parameters
        {
            public bool Invert;
            public string Name;
            public string Format;
            public string Version;
            public string Description;
            public string Type;
            public bool Overwrite;
        }

        private string baseFileName;
        private long maxFileSize;
        private Parameters parameters;

        private MBTiles currentFile;
        private List<MBTiles> tileFiles = null;

        private readonly object syncLock = new object();

        public MultiMBTiles(string baseFileName, long maxFileSize, bool invert = true, string name = "Gina Map Tiles", string format = "png", string version = "1.0.0", string description = "Gina Map Tiles", string type = "baselayer", bool overwrite = false)
        {
            this.baseFileName = baseFileName;
            this.maxFileSize = maxFileSize;
            
            this.parameters = new Parameters();
            this.parameters.Invert = invert;
            this.parameters.Name = name;
            this.parameters.Format = format;
            this.parameters.Version = version;
            this.parameters.Description = description;
            this.parameters.Type = type;
            this.parameters.Overwrite = overwrite;

        }

        
        public List<MBTiles> TileFiles
        {
            get
            {
                if (this.tileFiles == null) this.populateFileList();
                return this.tileFiles;
            }
        }

        /// <summary>
        /// returns file name for given file index in format baseFilenameXX.mbtiles, where XX is index in double decimal 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private string getFileName(int index)
        {
            return String.Format("{0}{1}.mbtiles", this.baseFileName, index.ToString("D2"));
        }

        /// <summary>
        /// Loads all existing files into this.Files
        /// </summary>
        private void populateFileList()
        {
            this.tileFiles = new List<MBTiles>();
            int index = 0;
            while(true) {
                string fileName = this.getFileName(index);

                if (! File.Exists(fileName)) break;

                MBTiles file = new MBTiles(
                    fileName, 
                    this.parameters.Invert, 
                    this.parameters.Name, 
                    this.parameters.Format, 
                    this.parameters.Version, 
                    this.parameters.Description, 
                    this.parameters.Type, 
                    this.parameters.Overwrite
                );

                this.tileFiles.Add(file);
                index++;
            }

        }

        /// <summary>
        /// Add new file to list and set it to be the current file
        /// </summary>
        /// <returns></returns>
        private MBTiles addFile() 
        {
            string fileName = this.getFileName(this.TileFiles.Count);

            MBTiles file = new MBTiles(
                    fileName,
                    this.parameters.Invert,
                    this.parameters.Name,
                    this.parameters.Format,
                    this.parameters.Version,
                    this.parameters.Description,
                    this.parameters.Type,
                    this.parameters.Overwrite
                );
            
            this.TileFiles.Add(file);
            return file;
            
        }


        private MBTiles getFirstFreeFile()
        {
            
            // if current file is not full yet, we can return it directly
            if (this.currentFile != null && this.currentFile.FileSize < this.maxFileSize)
            {
                return this.currentFile;
            }

            // search for first empty file
            for (int i = 0; i < this.TileFiles.Count; i++)
            {
                MBTiles file = this.TileFiles[i];
                if (file.FileSize < this.maxFileSize)
                {
                    return file;
                }
            }

            this.addFile();
            this.currentFile = this.TileFiles.Last();
            return this.currentFile;
            
        }


        public void StoreTile(Stream tileStream, int x, int y, int zoom)
        {
            lock (this.syncLock)
            {
                MBTiles tiles = getFirstFreeFile();
                tiles.StoreTile(tileStream, x, y, zoom);
            }
        }


        public bool Exists(int x, int y, int zoom)
        {
            lock (this.syncLock)
            {
                foreach (MBTiles tiles in this.TileFiles)
                {
                    if (tiles.Exists(x, y, zoom)) return true;
                }
                return false;
            }
        }

        public Stream GetTileStream(int x, int y, int zoom)
        {
            lock (this.syncLock)
            {
                foreach (MBTiles tiles in this.TileFiles)
                {
                    if (tiles.Exists(x, y, zoom))
                    {
                        return tiles.GetTileStream(x, y, zoom);
                    }
                }
                return null;
            }
        }
    }
}
